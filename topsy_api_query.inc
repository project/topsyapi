<?php
class topsy_api_query extends rest_api_query {
  // Only use the secure API
  // protected $scheme = 'https';
  // The base path of the API is foobar.local/services/rest/version-3
  protected $host = 'otter.topsy.com';
  // protected $path = 'services/rest/version-3';
  // Foobar authenticates via BasicAuth using the API key as username and "api_token" as password.
  // protected $password = 'api_token';
  // function __construct($api_key = NULL) {
  //   $this->username = $api_key ? $api_key : variable_get('foobar_default_api_key', FALSE);
  //   $this->initialized = !empty($this->username);
  // }
  

  /**
   * Fetches ALL the Redmine items, despite the limits of Redmine's pager.
   *
   * @see redmine_api_fetch_all()
   */
  function fetch_all($pages) {
    // Initialize the offset and make it easily accessible.
    $this->parameters['page'] = 1;
    $page = &$this->parameters['page'];

    // Use offset as the index to control iteration.
    for ($return = array(); empty($pages) || $page <= $pages; $page++) {
      // Execute the query and merge in the result set.
      if ($this->execute()->success && is_array($this->result->response->list)) {
        if (empty($this->result->response->list)) {
          return $return;
        }

        $return = array_merge($return, $this->result->response->list);
      }
      else {
        rve('error');
        // This case indicates an error, such as 404 or 403 or an error in the
        // API and/or iteration logic.
      }
    }

    return $return;
  }
}
